# Coder`s Lab prezentacja i zadania #

### Instalacja ###
1. Stwórz katalog, w którym chcesz mieć prezentację ```mkdir coderslabjs```.
2. Przejdź do katalogu ```cd coderslabjs```.
3. Sklonuj dwa repozytoria [prezentacja](https://adelura@bitbucket.org/codersclub/prezentacja.git) i [coders-lab-zadania-bez-rozwiazan](https://adelura@bitbucket.org/codersclub/coders-lab-zadania-bez-rozwiazan.git) za pomocą komendy ```git clone```.
4. Zmień nazwę katalogu ```mv coders-lab-zadania-bez-rozwiazan zadania```.
5. Otwórz plik prezentacji ```prezentacja/index.html```.